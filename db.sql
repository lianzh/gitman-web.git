/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.53 : Database - gitman_web
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gitman_web` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `gitman_web`;

/*Table structure for table `gitman_deploy` */

DROP TABLE IF EXISTS `gitman_deploy`;

CREATE TABLE `gitman_deploy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(10) unsigned NOT NULL COMMENT '仓库标识',
  `name` varchar(80) NOT NULL COMMENT '分支发布名称',
  `webhook_branch_ref` varchar(160) NOT NULL COMMENT 'webhook的钩子引用的分支标识,用于标明提交的时哪个分支',
  `branch_origin` varchar(80) NOT NULL COMMENT '远程分支的名称(用于执行 git pull origin 分支名称)',
  `code_dir` varchar(240) NOT NULL COMMENT '本地代码目录',
  `webhook_password` varchar(32) NOT NULL COMMENT 'webhook的钩子调用的密码校验',
  `extra_commands` text COMMENT '签出代码之后执行的命令,多条命令使用 ;;; 分隔',
  `xstatus` tinyint(4) NOT NULL DEFAULT '1' COMMENT '启用状态: 1 启用; 0 禁止',
  `created_at` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC COMMENT='仓库分支发布';

/*Table structure for table `gitman_hookrecord` */

DROP TABLE IF EXISTS `gitman_hookrecord`;

CREATE TABLE `gitman_hookrecord` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` int(10) unsigned NOT NULL COMMENT '创建时间',
  `deploy_id` int(10) unsigned NOT NULL COMMENT '分支部署标识',
  `repository_id` int(10) unsigned NOT NULL COMMENT '仓库标识',
  `do_status` tinyint(8) NOT NULL DEFAULT '0' COMMENT '执行状态: 0 未开始; 1 执行中; 2 执行完成; 3执行失败',
  `do_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行时间',
  `do_msg` varchar(240) NOT NULL DEFAULT '' COMMENT '执行时记录的信息(诸如报错)',
  `commits_info` longtext COMMENT '提交代码数据(json格式)',
  `webhook_name` varchar(120) NOT NULL COMMENT 'webhook_name',
  `webhook_type` tinyint(8) NOT NULL DEFAULT '0' COMMENT 'webhook_type: 1 push; 2 tagPush; 3 issue; 4 pullRequest; 5 comment',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC COMMENT='webhook 信息聚合表';

/*Table structure for table `gitman_repository` */

DROP TABLE IF EXISTS `gitman_repository`;

CREATE TABLE `gitman_repository` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL COMMENT '名称',
  `url` varchar(240) NOT NULL COMMENT 'url:项目的url,唯一',
  `git_http_url` varchar(240) NOT NULL COMMENT 'git_http_url',
  `git_ssh_url` varchar(240) NOT NULL COMMENT 'git_ssh_url',
  `platform` tinyint(8) NOT NULL COMMENT '仓库平台: 1 gitee; 2 github; 3 自建服务',
  `description` text COMMENT '仓库描述',
  `created_at` int(10) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_index` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC COMMENT='代码仓库表';


ALTER TABLE `gitman_deploy` ADD COLUMN `mode` TINYINT(4) DEFAULT 1 NOT NULL COMMENT '部署模式: 1 本地; 2 远程' AFTER `branch_origin`;
ALTER TABLE `gitman_hookrecord` ADD COLUMN `mode` TINYINT(4) DEFAULT 0 NOT NULL COMMENT '部署模式: 0 历史; 1 本地; 2 远程' AFTER `repository_id`;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
